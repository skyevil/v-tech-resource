module.exports = {
  purge: [
    './src/views/**/*.vue',
    './src/components/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'hy-yellow':'#FFFF15'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
